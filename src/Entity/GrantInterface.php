<?php

namespace Drupal\entity_grants\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining an entity.
 */
interface GrantInterface extends ContentEntityInterface {

}
